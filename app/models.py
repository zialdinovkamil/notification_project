import time
import pytz
import datetime
from django.db import models
from django.utils import timezone
from django.db.models import Prefetch
from django.core.validators import RegexValidator


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    # phone_regex = RegexValidator(regex=r'^7\d{10}$')
    phone_number  = models.CharField(unique=True, max_length=11)
    operator_code = models.CharField(max_length=3)
    tag           = models.CharField(max_length=128)
    time_zone     = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    def to_dict(self):
        return {
            'id'           : self.pk,
            'phone_number' : self.phone_number,
            'operator_code': self.operator_code,
            'tag'          : self.tag,
            'time_zone'    : self.time_zone,
        }

    def __str__(self):
        return f'<Client[id:{self.pk}, phone_number:{self.phone_number}]>'


class Message(models.Model):
    STATUS = [
        ('done', 'done'),
        ('heading', 'heading')
    ]
    creat_at = models.DateTimeField(auto_now_add=True)
    status   = models.CharField(max_length=128, choices=STATUS)
    client   = models.ForeignKey(Client, on_delete=models.CASCADE)
    mailing  = models.ForeignKey("Mailing", on_delete=models.CASCADE)

    def to_dict(self):
        return {
            'id'      : self.pk,
            'creat_at': self.creat_at,
            'status'  : self.status,
            'client'  : self.client.to_dict(),
            'mailing' : self.mailing.to_dict(),
        }

    def __str__(self):
        return f'<Message[id:{self.pk}, status:{self.status}]>'


class Mailing(models.Model):
    text                 = models.TextField(max_length=1000)
    start_at             = models.DateTimeField(default=datetime.datetime.now)
    end_at               = models.DateTimeField(default=datetime.datetime.now)
    tag                  = models.CharField(max_length=128, blank=True)
    mobile_operator_code = models.CharField(max_length=3, blank=True)

    @staticmethod
    def prefetch(q):
        q = q.prefetch_related(Prefetch('message_set', queryset=Message.objects.order_by('status')))
        return q

    @property
    def dispatch_check(self):
        if self.start_at <= timezone.now() <= self.end_at:
            return True
        else:
            return False

    def to_dict(self):
        return {
            'id'           : self.pk,
            'text'         : self.text,
            'tag'          : self.tag,
            'operator_code': self.mobile_operator_code,
            'start_at'     : self.start_at,
            'end_at'       : self.end_at,
        }

    def to_dict_with_messages(self):
        return {
            'id'           : self.pk,
            'text'         : self.text,
            'tag'          : self.tag,
            'operator_code': self.mobile_operator_code,
            'start_at'     : self.start_at,
            'end_at'       : self.end_at,
            'messages'     : [i.to_dict() for i in self.message_set.all()],
        }

    def __str__(self):
        return f'<Mailing[id:{self.pk}, tag:{self.tag}]>'
