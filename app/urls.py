from django.urls import path, re_path
from rest_framework_swagger.views import get_swagger_view
import app.api.client as cl
import app.api.mailing as ml
import app.api.message as mg
import app.api.analytic as al

schema_view = get_swagger_view(title='Pastebin API')

app_name = 'app'
urlpatterns = [
    re_path(r'^$', schema_view),
    path('client', cl.client),
    path('client/<int:pk>', cl.client),
    path('mailing', ml.mailing),
    path('mailing/<int:pk>', ml.mailing),
    path('message', mg.message),
    path('message/<int:pk>', mg.message),
    path('detailed_statistics/<int:pk>', al.detailed_statistics),
    path('general_statistic', al.general_statistic)
]