from app.models import *
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def mailing(request, pk=None):
    if request.method == "GET":
        obj = Mailing.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        req = json.loads(request.body)
        obj = Mailing()

        for k, v in req.items():
            if k in ['pk']:
                continue
            setattr(obj, k, v)


        obj.save()
        obj.refresh_from_db()

        return JsonResponse(obj.to_dict(), safe=False)

    elif request.method == "PATCH":
        req = json.loads(request.body)
        obj = Mailing.objects.get(pk=pk)

        update_fields = []
        for k, v in req.items():
            if k in ['pk']:
                continue
            setattr(obj, k, v)
            update_fields.append(k)

        if update_fields:
            obj.save(update_fields=update_fields)
            obj.refresh_from_db()

        return JsonResponse(obj.to_dict(), safe=False)

    elif request.method == 'DELETE':
        Mailing.objects.filter(pk=pk).delete()
        return JsonResponse({}, safe=False)


