from app.models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def detailed_statistics(req, pk=None):
    q = Mailing.objects.filter(pk=pk)
    q = Mailing.prefetch(q)
    resp = [obj.to_dict_with_messages() for obj in q.all()]
    return JsonResponse(resp, safe=False)


@csrf_exempt
def general_statistic(req):
    count_mailing = Mailing.objects.count()
    mailing = Mailing.objects.values('id')
    resp = {'Total mailings': count_mailing,
            'Messages sent': ''}
    result = {}

    for row in mailing:
        res = {'Total messages': 0, 'Done': 0, 'Heading': 0}
        mail = Message.objects.filter(mailing_id=row['id']).all()
        done_messages = mail.filter(status='done').count()
        heading_messages = mail.filter(status='heading').count()
        res['Total messages'] = len(mail)
        res['Done'] = done_messages
        res['Heading'] = heading_messages
        result[row['id']] = res

    resp['Messages sent'] = result
    return JsonResponse(resp, safe=False)
