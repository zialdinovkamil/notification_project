from app.models import *
from app.tasks import *
from django.http import JsonResponse
from django.db.models import Q
import json
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def message(request, pk=None):
    if request.method == "GET":
        obj = Message.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        req = json.loads(request.body)
        mailing = Mailing.objects.filter(pk=int(req["mailing"])).first()
        clients = Client.objects.filter(Q(operator_code=mailing.mobile_operator_code) |
                                        Q(tag=mailing.tag)).all()
        resp = []
        for client in clients:
            obj = Message.objects.create(
                status="heading",
                client_id=client.id,
                mailing_id=mailing.id
            )
            resp.append(obj.to_dict())
            message = Message.objects.filter(mailing_id=mailing.id, client_id=client.id, status='heading').first()
            data = {
                'id': message.id,
                "phone": client.phone_number,
                "text": mailing.text
            }
            client_id = client.id
            mailing_id = mailing.id
            if mailing.dispatch_check:
                send_message.apply_async((data, client_id, mailing_id),
                                         expires=mailing.end_at)
            else:
                send_message.apply_async((data, client_id, mailing_id),
                                         eta=mailing.start_at, expires=mailing.end_at)
        return JsonResponse(resp, safe=False)

    elif request.method == 'DELETE':
        Message.objects.filter(pk=pk).delete()
        return JsonResponse({}, safe=False)


