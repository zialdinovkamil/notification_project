SHELL = /bin/sh

.DEFAULT_GOAL := help


help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

up: ## Start all containers (in background) for development
	docker-compose up -d --build
	docker logs -f notification-web-1

down: ## Stop all started for development containers
	docker-compose down

restart_web: ## Web DB migrate
	docker restart notification-web-1
	docker logs -f notification-web-1

migrations: ## Web DB migrate
	docker exec -it notification-web-1 python3 manage.py makemigrations
	docker exec -it notification-web-1 python3 manage.py makemigrations app
	docker exec -it notification-web-1 python3 manage.py migrate

createsuperuser: ## Create super user
	docker exec -it notification-web-1 python3 manage.py createsuperuser
